import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { AnnotationHelper } from './annotation-helper';
import { exampleAnnotationDatas } from './example-annotation-data';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'PdfHighlighting';

  @ViewChild('pdf', {static: false}) public pdf: any;

  pdfJsViewerPath = '../assets/pdfjs/web/viewer.html';
  pdfJsSrc?: SafeResourceUrl;

  private annotationDatas = exampleAnnotationDatas;
  private annotationHelper?: AnnotationHelper;

  constructor(private sanitizer: DomSanitizer, private http: HttpClient) {
    this.loadPdfFromLocalFile();
  }

  loadPdfFromLocalFile(): void {
    // const localFilePath = '../assets/pdfjs/web/compressed.tracemonkey-pldi-09.pdf';
    const localFilePath = '../assets/Sample.pdf';
    const loadLocalFileSub = this.http.get(localFilePath, {responseType: 'blob'}).subscribe((blob: Blob) => {
      this.loadPdfFromBlob(blob);
      loadLocalFileSub.unsubscribe();
    });
  }

  loadPdfFromBlob(blob: Blob): void {
    this.pdfJsSrc = this.sanitizer.bypassSecurityTrustResourceUrl(this.pdfJsViewerPath + '?file=' + URL.createObjectURL(blob));
  }

  ngAfterViewInit(): void {
    const initTask = setInterval(() => {
      const pdfViewerApplication = this.pdf.nativeElement.contentWindow.PDFViewerApplication;
      if (pdfViewerApplication) {
        this.annotationHelper = new AnnotationHelper(this.pdf, this.annotationDatas);
        clearInterval(initTask);
      }
    }, 100);
  }

  setDefaultAnnotations(): void {
    this.annotationHelper?.setVisibleAnnotations('original');
  }

  setOcrAnnotations(): void {
    this.annotationHelper?.setVisibleAnnotations('custom');
  }

  setAllAnnotations(): void {
    this.annotationHelper?.setVisibleAnnotations('all');
  }

  setNoAnnotations(): void {
    this.annotationHelper?.setVisibleAnnotations('none');
  }
}
