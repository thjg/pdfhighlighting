import { AnnotationData } from "./annotation-helper";

export const exampleAnnotationDatas: AnnotationData[] = [
  {
    page: 1,
    rect: [10, 10, 60, 60],
    style: 'none',
    color: new Uint8ClampedArray([255, 0, 0]),
    url: 'https://google.com',
  },
  {
    page: 1,
    rect: [80, 10, 130, 60],
    style: 'box',
    color: new Uint8ClampedArray([0, 0, 255]),
    url: 'https://gitlab.com/thjg',
  },
  {
    page: 1,
    rect: [50, 500, 100, 550],
    style: 'fill',
    color: new Uint8ClampedArray([255, 255, 0]),
    header: 'Achtung',
    text: 'Fill-Annotation',
  },
  {
    page: 1,
    rect: [200, 500, 300, 600],
    style: 'box',
    color: new Uint8ClampedArray([0, 255, 0]),
    header: 'OK',
    text: 'Box-Annotation',
  },
  {
    page: 2,
    rect: [200, 500, 300, 600],
    style: 'box',
    color: new Uint8ClampedArray([0, 255, 0]),
    header: 'OK2',
    text: 'Box-Annotation',
  },
];