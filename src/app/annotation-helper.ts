import { Buffer } from "buffer";

export type AnnotationStyle = 'box' | 'fill' | 'none';
export interface AnnotationData {
  id?: string;
  page: number;
  rect: number[]; // X(left, from left) Y(bottom, from bottom) X(right, from left) Y(top, from bottom) || [0, 0, 612, 792]
  style: AnnotationStyle;
  color: Uint8ClampedArray;
  url?: string;
  header?: string;
  text?: string;
}

export class AnnotationHelper {
  private pdfViewerApplication: any;
  private headHtml: HTMLElement;
  private annotationIdCounter = 1;
  private originalPdfAnnotations: { [pageNumber: number]: any[] } = {};
  private customPdfAnnotations: { [pageNumber: number]: any[] } = {};
  private pagesCount = -1;

  constructor(iframe: any, private annotationDatas: AnnotationData[]) {
    this.pdfViewerApplication = iframe.nativeElement.contentWindow.PDFViewerApplication;
    this.headHtml = iframe.nativeElement.contentWindow.document.head;
    this.hideToolbarButtons();
    this.updateShowCustomAnnotationsButton(false);
    this.pdfViewerApplication.initializedPromise.then(() => {
      const pagesLoadedTask = setInterval(async () => {
        const pagesCount = this.pdfViewerApplication.pdfDocument.numPages;
        if (pagesCount > 0) {
          clearInterval(pagesLoadedTask);
          this.pagesCount = pagesCount;
          for (let i = 1; i <= this.pagesCount; i++) {
            this.originalPdfAnnotations[i] = await this.getPdfAnnotations(i);
          }
          for (const annotation of this.annotationDatas) {
            if (!this.customPdfAnnotations[annotation.page]) this.customPdfAnnotations[annotation.page] = [];
            this.customPdfAnnotations[annotation.page].push(this.createPdfAnnotation(annotation));
          }
        }
      }, 100);
    });
  }

  private hideToolbarButtons(): void {
    const toolbar = this.pdfViewerApplication.appConfig.toolbar;
    const secondaryToolbar = this.pdfViewerApplication.appConfig.secondaryToolbar;

    toolbar.editorFreeTextButton.hidden = true;
    toolbar.editorInkButton.hidden = true;
    toolbar.editorStampButton.hidden = true;

    secondaryToolbar.openFileButton.hidden = true;
    secondaryToolbar.viewBookmarkButton.hidden = true;
  }

  private updateShowCustomAnnotationsButton(customAnnotationsVisible: boolean): void {
    const style = document.createElement('style');
    style.innerText = '#showCustomAnnotationsButton::before { background-color: transparent; }';
    style.id = 'showCustomAnnotationsButtonStyle';
    if (!this.headHtml.querySelector('#showCustomAnnotationsButtonStyle')) this.headHtml.appendChild(style);

    const toolbar = this.pdfViewerApplication.appConfig.toolbar;
    if (!toolbar.showCustomAnnotationsButton) toolbar.showCustomAnnotationsButton = document.createElement('button');
    const showCustomAnnotationsButton = toolbar.showCustomAnnotationsButton;
    showCustomAnnotationsButton.id = 'showCustomAnnotationsButton';
    showCustomAnnotationsButton.textContent = 'OCR';
    showCustomAnnotationsButton.onclick = () => customAnnotationsVisible ? this.setVisibleAnnotations('original') : this.setVisibleAnnotations('all');
    showCustomAnnotationsButton.title = customAnnotationsVisible ? 'OCR Daten verstecken' : 'OCR Daten anzeigen';
    showCustomAnnotationsButton.classList.add('toolbarButton');
    showCustomAnnotationsButton.style.width = 'auto';
    if (customAnnotationsVisible) showCustomAnnotationsButton.style.backgroundColor = 'var(--toggled-btn-bg-color)';
    else showCustomAnnotationsButton.style.backgroundColor = null;

    const toolbarViewerRight = toolbar.container.children[1];
    if (toolbarViewerRight.firstChild !== showCustomAnnotationsButton) toolbarViewerRight.insertBefore(showCustomAnnotationsButton, toolbarViewerRight.firstChild);
  }

  public setVisibleAnnotations(scope: 'all' | 'original' | 'custom' | 'none', pageNumber?: number): void {
    this.clearVisibleAnnotations(pageNumber);
    const setVisibleFn = (pageNumber: number) => {
      const annotationsToShow = [];
      if (scope === 'all') {
        annotationsToShow.push(...(this.originalPdfAnnotations[pageNumber] ?? []), ...(this.customPdfAnnotations[pageNumber] ?? []));
        this.updateShowCustomAnnotationsButton(true);
      } else if (scope === 'original') {
        annotationsToShow.push(...(this.originalPdfAnnotations[pageNumber] ?? []));
        this.updateShowCustomAnnotationsButton(false);
      } else if (scope === 'custom') {
        annotationsToShow.push(...(this.customPdfAnnotations[pageNumber] ?? []));
        this.updateShowCustomAnnotationsButton(true);
      } else {
        this.updateShowCustomAnnotationsButton(false);
      }
      if (annotationsToShow.length > 0) this.drawVisibleAnnotations(pageNumber, annotationsToShow);
    };
    if (pageNumber) setVisibleFn(pageNumber);
    else for (let i = 1; i <= this.pagesCount; i++) { setVisibleFn(i); }
  }

  private clearVisibleAnnotations(pageNumber?: number): void {
    const clearFn = (pageNumber: number) => {
      const annotationLayer = this.pdfViewerApplication.pdfViewer._pages[pageNumber - 1].annotationLayer?.annotationLayer;
      if (annotationLayer) annotationLayer.div.innerHTML = '';
    };
    if (pageNumber) clearFn(pageNumber);
    else for (let i = 1; i <= this.pagesCount; i++) { clearFn(i); }
  }

  private drawVisibleAnnotations(pageNumber: number, annotations: any[]): void {
    const annotationLayerBuilder = this.pdfViewerApplication.pdfViewer._pages[pageNumber - 1].annotationLayer;
    const annotationLayer = annotationLayerBuilder.annotationLayer;
    const renderContext = {
      annotations: annotations,
      linkService: annotationLayerBuilder.linkService,
      downloadManager: annotationLayerBuilder.downloadManager,
      imageResourcesPath: annotationLayerBuilder.imageResourcesPath,
      renderForms: annotationLayerBuilder.renderForms,
      annotationStorage: annotationLayerBuilder.annotationStorage,
      enableScripting: annotationLayerBuilder.enableScripting,
      hasJSActions: annotationLayerBuilder._hasJSActionsPromise,
      fieldObjects: annotationLayerBuilder._fieldObjectsPromise,
    };
    annotationLayer.render(renderContext);

    for (const element of annotationLayer.div.children) {
      const elementId: string = element.getAttribute('data-annotation-id');
      if (elementId.startsWith('be-annotation-fill')) {
        const color = this.annotationDatas.find(annotationData => annotationData.id === elementId)!!.color;
        element.style.backgroundColor = `${this.uint8ToHex(color)}50`;
      } else if (elementId.startsWith('be-annotation-box')) {
        const color = this.annotationDatas.find(annotationData => annotationData.id === elementId)!!.color;
        element.style.border = `2px solid ${this.uint8ToHex(color)}`;
      }
    }
  }

  private async getPdfPage(pageNumber: number): Promise<any> {
    return await this.pdfViewerApplication.pdfDocument.getPage(pageNumber);
  }

  private async getPdfAnnotations(pageNumber: number): Promise<any> {
    return await (await this.getPdfPage(pageNumber)).getAnnotations();
  }

  private nextAnnotationId(style: AnnotationStyle): string {
    return `be-annotation-${style}_${this.annotationIdCounter++}`;
  }

  private createPdfAnnotation(annotationData: AnnotationData): any {
    annotationData.id = this.nextAnnotationId(annotationData.style);
    if (annotationData.url) return AnnotationCreator.createLink(annotationData);
    else if (annotationData.style === 'box') return AnnotationCreator.createSquare(annotationData);
    else if (annotationData.style === 'fill') return AnnotationCreator.createHighlight(annotationData);
  }

  private uint8ToHex(uint8: Uint8ClampedArray): string {
    return `#${Buffer.from(uint8).toString('hex')}`;
  }
}

namespace AnnotationCreator {
  function createBaseAnnotation(annotationData: AnnotationData): any {
    return {
      annotationFlags: null,
      annotationType: null,
      backgroundColor: null,
      borderColor: null,
      borderStyle: {
        dashArray: [3],
        horizontalCornerRadius: 0,
        style: 1,
        verticalCornerRadius: 0,
        width: 0,
      },
      color: null,
      contentsObj: {
        dir: 'ltr',
        str: '',
      },
      creationDate: null,
      hasAppearance: false,
      hasOwnCanvas: false,
      id: annotationData.id,
      modificationDate: null,
      noHTML: false,
      noRotate: false,
      popupRef: null,
      rect: annotationData.rect,
      rotation: 0,
      subtype: null,
      titleObj: {
        dir: 'ltr',
        str: '',
      },
      unsafeUrl: null,
      url: null,
    };
  }

  export function createLink(annotationData: AnnotationData): any {
    const annotation = createBaseAnnotation(annotationData);
    annotation.annotationFlags = 0;
    annotation.annotationType = 2; // Link
    annotation.subtype = 'Link';
    annotation.unsafeUrl = annotationData.url;
    annotation.url = annotationData.url;
    return annotation;
  }

  export function createSquare(annotationData: AnnotationData): any {
    const annotation = createBaseAnnotation(annotationData);
    annotation.annotationFlags = 4;
    annotation.annotationType = 5; // Square
    annotation.color = annotationData.color;
    if (annotationData.text) annotation.contentsObj.str = annotationData.text;
    annotation.hasAppearance = true;
    annotation.subtype = 'Square';
    if (annotationData.header) annotation.titleObj.str = annotationData.header;
    return annotation;
  }

  export function createHighlight(annotationData: AnnotationData): any {
    const annotation = createBaseAnnotation(annotationData);
    annotation.annotationFlags = 4;
    annotation.annotationType = 9; // Highlight
    annotation.color = annotationData.color;
    if (annotationData.text) annotation.contentsObj.str = annotationData.text;
    annotation.hasAppearance = true;
    annotation.subtype = 'Highlight';
    if (annotationData.header) annotation.titleObj.str = annotationData.header;
    return annotation;
  }
}